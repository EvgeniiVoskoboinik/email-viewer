angular
    .module('app')
    .service('emailService', emailService);

emailService.$inject = ['$http'];

function emailService($http){
    var emails;
    return{
        getEmails: function(callback){
            if (emails){
                return emails;
            }
            $http.get('data/email.json')
                .then(function(res){
                    emails = res.data;
                    callback(emails);
                }, function(err){
                })
        }
    }
}