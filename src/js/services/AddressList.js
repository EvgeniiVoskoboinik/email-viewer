angular
    .module('app')
    .service('addressList', addressList);

addressList.$inject = ['emailService'];

function addressList(emailService){
    var addressList = [{val:'', label: 'Nothing selected'}];

    emailService.getEmails(function(res){
        var addrs = Object.create(null);

        res.forEach(function(email){
            addrs[email.from] = true;
            email.to.forEach(function(t){
                addrs[t] = true;
            })
        });
        for(var prop in addrs){
           addressList.push({val: prop, label: prop});
        }
    });

    return{
        get: function(){
            return addressList;
        }
    }
}