angular
    .module('app')
    .filter('dateFilter', dateFilter);

function dateFilter() {
    return function(input, query) {
        var from, to;

        from = new Date(query.from);
        to = new Date(query.to);

        from = from.toString() !== 'Invalid Date' ? from : null;
        to = to.toString() !== 'Invalid Date' ? to : null;

        return input.filter(function(mail){
            var mail_date = new Date(mail.date);
            return (!from || mail_date >= from) && (!to || mail_date <= to)
        });
    }
}