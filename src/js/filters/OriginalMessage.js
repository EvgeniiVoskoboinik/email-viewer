angular
    .module("app")
    .filter('originalMessage', originalMessage);

function originalMessage(){
    return function(input){
        if(!input) return;

        var messages = input.split('-----Original Message-----');

         return messages.reduceRight(function(sum, current, ind){
            if (ind === 0) return current + sum;
            return '<div class="original-message"><span ng-click="vm.toggle($event);">-----Original Message-----</span><pre>' + current + sum + '</pre></div>';
        }, '');
    }
}