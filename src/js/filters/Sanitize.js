angular
    .module("app")
    .filter('sanitize', sanitize);

sanitize.$inject = ['$sce', 'linkyFilter'];

function sanitize($sce, linkyFilter){

    var ELEMENT_NODE = 1;
    var TEXT_NODE = 3;
    var editedDOM = document.createElement('div');
    var inputDOM = document.createElement('div');

    function beautifuly(startNode, nopre){

        var i, currentNode, len;

        for (i = 0, len = startNode.childNodes.length; i < len; i++) {
            currentNode = startNode.childNodes[i];
            switch (currentNode.nodeType) {
                case ELEMENT_NODE:{
                    beautifuly(currentNode, true);
                    break;
                }
                case TEXT_NODE:{
                    if(nopre){
                        editedDOM.innerHTML = linkyFilter(currentNode.textContent);
                    }
                    else{
                        editedDOM.innerHTML = '<pre>' + linkyFilter(currentNode.textContent) + '</pre>';
                    }
                    i += editedDOM.childNodes.length - 1;
                    while(editedDOM.childNodes.length) {
                        startNode.insertBefore(editedDOM.childNodes[0], currentNode);
                    }
                    startNode.removeChild(currentNode);
                }
            }
        }
        return startNode;
    }

    return function(htmlCode){
        if(!htmlCode) return;
        inputDOM.innerHTML = $sce.trustAsHtml(htmlCode);
        return beautifuly(inputDOM).innerHTML;
    }
}