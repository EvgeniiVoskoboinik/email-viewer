angular
    .module('app')
    .filter('paginationFilter', paginationFilter);

function paginationFilter() {
    return function(input, query) {
        var start = query.currentPage * query.pageSize;
        var stop = start + query.pageSize;
        return input.slice(start,stop);
    }
}