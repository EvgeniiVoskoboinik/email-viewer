angular
    .module('app')
    .filter('customFilter', customFilter);

function customFilter() {
    return function(input, query) {
        var from, to;

        from = new Date(query.date.from);
        to = new Date(query.date.to);

        from = from.toString() !== 'Invalid Date' ? from : null;
        to = to.toString() !== 'Invalid Date' ? to : null;

        return input.filter(function(mail){
            var mail_date = new Date(mail.date);

            if(from && mail_date < from)
                return false;
            if(to && mail_date > to)
                return false;
            if(query.mail_address !== '' && (mail.from !== query.mail_address && !mail.to.some(function(t){ return t === query.mail_address})))
                return false;
            if(query.text !== '' && (!~mail.subject.toLowerCase().indexOf(query.text) && !~mail.body.toLowerCase().indexOf(query.text)))//will not properly work for html
                return false;

            return true;
        });
    }
}