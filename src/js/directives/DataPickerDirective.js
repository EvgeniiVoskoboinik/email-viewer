angular
    .module('app')
    .directive('datapicker', dataPickerDirective);

function dataPickerDirective() {
    return {
        restrict: 'A',
        scope:{
          datapicker: '='
        },
        link: function(scope, element, attrs) {

            $(element).datetimepicker({
                viewMode: 'years',
                useCurrent: false,
                format: 'L'
            });
            $(element).on("dp.change", function (e) {
                scope.datapicker = e.date;
                scope.$apply();
            });

        }
    };
}