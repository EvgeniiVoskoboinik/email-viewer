angular
    .module('app')
    .directive('highlight', highlight);

function highlight() {
    return {
        restrict: 'A',
        scope:{
            highlight: '=',
            mail: '='
        },
        link: function(scope, element, attrs) {

            scope.$watch('mail', function(n){
                if (!n) return;
                hl(true);
            }, true);

            element.val(scope.highlight);
            element.data('old-value', scope.highlight);

            scope.$watch('highlight', function (val) {
                element.val(scope.highlight);
            });

            element.bind('propertychange keyup paste search', hl);


            function hl(fl){
                var search_text = $('.search-text');
                var val = element.val();
                if (fl || element.data('old-value') != val) {

                    search_text.removeHighlight();

                    if(val !== ''){
                        search_text.highlight(val);
                    }
                    scope.highlight = val;
                    element.data('old-value', val);

                    var phase = scope.$root.$$phase;
                    if (phase != '$apply' && phase != '$digest') {
                        scope.$apply();
                    }
                }
            }
        }
    };
}