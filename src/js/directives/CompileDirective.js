angular
    .module('app')
    .directive('compile', compile);

compile.$inject = ['$compile'];

function compile($compile) {
    return function(scope, element, attrs) {

        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.compile);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        )};
}
