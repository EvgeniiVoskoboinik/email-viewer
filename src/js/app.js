angular
    .module('app', ['ngSanitize'])
    .controller('appCtrl', appCtrl);

appCtrl.$inject = ['$scope', 'emailService', 'addressList'];

function appCtrl($scope, emailService, addressList){
    var vm = this;

    vm.emails = [];
    vm.pagination = {
        currentPage: 0,
        pageSize: 20
    };
    vm.search = {
        mail_address:'',
        date:{
            from:'',
            to:''
        },
        text:''
    };
    vm.cMail = null;
    vm.addresses = addressList.get();

    vm.toggle = toggle;

    $scope.$watch('filteredEmails.length', function(n,o){
        if(!n) return;
        if(n!==o){
           vm.pagination.currentPage = 0;
        }
    });

    function toggle(element){
        $(element.currentTarget).siblings().toggle();
    }

    (function(){
        emailService.getEmails(function(data){
            vm.emails = data;
        });
    })();

}