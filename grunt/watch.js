module.exports = {
    html: {
        files: ['src/**/*.html'],
        tasks: ['htmlmin']
    },
    js: {
        files: ['src/js/**/*'],
        tasks: ['concat:app_js']
    },
    css: {
        files: ['src/scss/**/*.css'],
        tasks: ['concat:app_css']
    },
    public: {
        files: ['public/**/*'],
        options: {
            debounceDelay: 200,
            livereload: true
        }
    }
};