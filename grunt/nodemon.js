module.exports = {
    dev: {
        script: 'run.js',
        options: {
            ignore: [
                'node_modules/**',
                'bower_components/**',
                'public/**/*',
                'grunt/**/*',
                'src/**'
            ],
            delay: 1000,
            callback: function (nodemon) {
                nodemon.on('log', function (event) {
                    console.log(event.colour);
                });
                nodemon.on('restart', function () {
                    setTimeout(function () {
                        require('fs').writeFileSync('.rebooted', 'rebooted');
                    }, 500);
                });
            }
        }
    }
};