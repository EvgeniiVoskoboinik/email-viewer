module.exports = {
    bower_js: {
        options: {
            separator: ';'
        },
        src: [
            'bower_components/jQuery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/angular/angular.min.js',
            'bower_components/angular-sanitize/angular-sanitize.min.js',
            'bower_components/moment/moment.js',
            'bower_components/moment-timezone/builds/moment-timezone-with-data-2010-2020.js',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
        ],
        dest: 'public//js/_b.js'
    },
    bower_css: {
        src: [
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
        ],
        dest: 'public/css/_b.css'
    },
    app_js: {
        options: {
            separator: ';'
        },
        src: ['src/js/**/*.js'],
        dest: 'public/js/app.js'
    },
    app_css: {
        src: [
            'src/scss/**/*.css'
        ],
        dest: 'public/css/app.css'
    }
};