module.exports = {
    data: {
        expand: true,
        cwd: 'src/data',
        src: 'email.json',
        dest: 'public/data'
    }
};
