angular
    .module('app', ['ngSanitize'])
    .controller('appCtrl', appCtrl);

appCtrl.$inject = ['$scope', 'emailService', 'addressList'];

function appCtrl($scope, emailService, addressList){
    var vm = this;

    vm.emails = [];
    vm.pagination = {
        currentPage: 0,
        pageSize: 20
    };
    vm.search = {
        mail_address:'',
        date:{
            from:'',
            to:''
        },
        text:''
    };
    vm.cMail = null;
    vm.addresses = addressList.get();

    vm.toggle = toggle;

    $scope.$watch('filteredEmails.length', function(n,o){
        if(!n) return;
        if(n!==o){
           vm.pagination.currentPage = 0;
        }
    });

    function toggle(element){
        $(element.currentTarget).siblings().toggle();
    }

    (function(){
        emailService.getEmails(function(data){
            vm.emails = data;
        });
    })();

};angular
    .module('app')
    .directive('compile', compile);

compile.$inject = ['$compile'];

function compile($compile) {
    return function(scope, element, attrs) {

        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.compile);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        )};
}
;angular
    .module('app')
    .directive('datapicker', dataPickerDirective);

function dataPickerDirective() {
    return {
        restrict: 'A',
        scope:{
          datapicker: '='
        },
        link: function(scope, element, attrs) {

            $(element).datetimepicker({
                viewMode: 'years',
                useCurrent: false,
                format: 'L'
            });
            $(element).on("dp.change", function (e) {
                scope.datapicker = e.date;
                scope.$apply();
            });

        }
    };
};angular
    .module('app')
    .directive('highlight', highlight);

function highlight() {
    return {
        restrict: 'A',
        scope:{
            highlight: '=',
            mail: '='
        },
        link: function(scope, element, attrs) {

            scope.$watch('mail', function(n){
                if (!n) return;
                hl(true);
            }, true);

            element.val(scope.highlight);
            element.data('old-value', scope.highlight);

            scope.$watch('highlight', function (val) {
                element.val(scope.highlight);
            });

            element.bind('propertychange keyup paste search', hl);


            function hl(fl){
                var search_text = $('.search-text');
                var val = element.val();
                if (fl || element.data('old-value') != val) {

                    search_text.removeHighlight();

                    if(val !== ''){
                        search_text.highlight(val);
                    }
                    scope.highlight = val;
                    element.data('old-value', val);

                    var phase = scope.$root.$$phase;
                    if (phase != '$apply' && phase != '$digest') {
                        scope.$apply();
                    }
                }
            }
        }
    };
};angular
    .module('app')
    .directive('selectpicker', selectpicker);

function selectpicker($timeout) {
    return {
        restrict: 'A',
        scope:{
            datapicker: '='
        },
        link: function(scope, element, attrs) {

            var button = $()
            $timeout(function(){
                $(element).selectpicker();
            }, 1000)




        }
    };
};angular
    .module('app')
    .filter('customFilter', customFilter);

function customFilter() {
    return function(input, query) {
        var from, to;

        from = new Date(query.date.from);
        to = new Date(query.date.to);

        from = from.toString() !== 'Invalid Date' ? from : null;
        to = to.toString() !== 'Invalid Date' ? to : null;

        return input.filter(function(mail){
            var mail_date = new Date(mail.date);

            if(from && mail_date < from)
                return false;
            if(to && mail_date > to)
                return false;
            if(query.mail_address !== '' && (mail.from !== query.mail_address && !mail.to.some(function(t){ return t === query.mail_address})))
                return false;
            if(query.text !== '' && (!~mail.subject.toLowerCase().indexOf(query.text) && !~mail.body.toLowerCase().indexOf(query.text)))//will not properly work for html
                return false;

            return true;
        });
    }
};angular
    .module('app')
    .filter('dateFilter', dateFilter);

function dateFilter() {
    return function(input, query) {
        var from, to;

        from = new Date(query.from);
        to = new Date(query.to);

        from = from.toString() !== 'Invalid Date' ? from : null;
        to = to.toString() !== 'Invalid Date' ? to : null;

        return input.filter(function(mail){
            var mail_date = new Date(mail.date);
            return (!from || mail_date >= from) && (!to || mail_date <= to)
        });
    }
};angular
    .module("app")
    .filter('originalMessage', originalMessage);

function originalMessage(){
    return function(input){
        if(!input) return;

        var messages = input.split('-----Original Message-----');

         return messages.reduceRight(function(sum, current, ind){
            if (ind === 0) return current + sum;
            return '<div class="original-message"><span ng-click="vm.toggle($event);">-----Original Message-----</span><pre>' + current + sum + '</pre></div>';
        }, '');
    }
};angular
    .module('app')
    .filter('paginationFilter', paginationFilter);

function paginationFilter() {
    return function(input, query) {
        var start = query.currentPage * query.pageSize;
        var stop = start + query.pageSize;
        return input.slice(start,stop);
    }
};angular
    .module("app")
    .filter('sanitize', sanitize);

sanitize.$inject = ['$sce', 'linkyFilter'];

function sanitize($sce, linkyFilter){

    var ELEMENT_NODE = 1;
    var TEXT_NODE = 3;
    var editedDOM = document.createElement('div');
    var inputDOM = document.createElement('div');

    function beautifuly(startNode, nopre){

        var i, currentNode, len;

        for (i = 0, len = startNode.childNodes.length; i < len; i++) {
            currentNode = startNode.childNodes[i];
            switch (currentNode.nodeType) {
                case ELEMENT_NODE:{
                    beautifuly(currentNode, true);
                    break;
                }
                case TEXT_NODE:{
                    if(nopre){
                        editedDOM.innerHTML = linkyFilter(currentNode.textContent);
                    }
                    else{
                        editedDOM.innerHTML = '<pre>' + linkyFilter(currentNode.textContent) + '</pre>';
                    }
                    i += editedDOM.childNodes.length - 1;
                    while(editedDOM.childNodes.length) {
                        startNode.insertBefore(editedDOM.childNodes[0], currentNode);
                    }
                    startNode.removeChild(currentNode);
                }
            }
        }
        return startNode;
    }

    return function(htmlCode){
        if(!htmlCode) return;
        inputDOM.innerHTML = $sce.trustAsHtml(htmlCode);
        return beautifuly(inputDOM).innerHTML;
    }
};jQuery.fn.highlight = function(pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
        var ELEMENT_NODE = 1;
        var TEXT_NODE = 3;
        if (node.nodeType == TEXT_NODE) {
            var pos = node.data.toUpperCase().indexOf(pat);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == ELEMENT_NODE && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.each(function() {
        innerHighlight(this, pat.toUpperCase());
    });
};

jQuery.fn.removeHighlight = function() {
    return this.find("span.highlight").each(function() {
        this.parentNode.firstChild.nodeName;
        with (this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};;angular
    .module('app')
    .service('addressList', addressList);

addressList.$inject = ['emailService'];

function addressList(emailService){
    var addressList = [{val:'', label: 'Nothing selected'}];

    emailService.getEmails(function(res){
        var addrs = Object.create(null);

        res.forEach(function(email){
            addrs[email.from] = true;
            email.to.forEach(function(t){
                addrs[t] = true;
            })
        });
        for(var prop in addrs){
           addressList.push({val: prop, label: prop});
        }
    });

    return{
        get: function(){
            return addressList;
        }
    }
};angular
    .module('app')
    .service('emailService', emailService);

emailService.$inject = ['$http'];

function emailService($http){
    var emails;
    return{
        getEmails: function(callback){
            if (emails){
                return emails;
            }
            $http.get('data/email.json')
                .then(function(res){
                    emails = res.data;
                    callback(emails);
                }, function(err){
                })
        }
    }
}